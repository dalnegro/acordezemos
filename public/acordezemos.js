// === DEFINIÇÃO DE CONSTANTES ===
const div_acorde=document.getElementById("acorde");
const div_acordeNome=document.getElementById("acordeNome");
const select_raizes=document.getElementById("raizes");
const select_acordes=document.getElementById("acordes");
const input_usarBemol=document.getElementById("usarBemol");
const span_formulaAcorde=document.getElementById("formulaAcorde");
const div_descricao=document.getElementById("descricao");
const div_partituraCompasso=document.getElementById("partituraCompasso");
const select_claves=document.getElementById("claves");
const table_teclado=document.getElementById("teclado");
const div_partitura=document.getElementById("partitura");

// === FUNÇÕES GERAIS ===
window.onresize=redimensionarTela;

// === DEFINIÇÃO DE VARIÁVEIS GLOBAIS ===
var exibicao="teclado";
var acordes=[];
var raizes=[];
var formulaInicial=null;

//Inicializa o programa, definindo variáveis ao valor inicial e chamando todas as funções referentes ao programa.
function inicializaAcordezemos() {
	{ //Inicialização de variáveis
		exibicao="teclado";
		acordes=[];
		raizes=[];
		formulaInicial=null;
	}
	{ //Inserção de raízes
		adicionaRaiz("0","C","Dó","C","Dó");
		adicionaRaiz("1","C#","Dó sustenido","Db","Ré bemol");
		adicionaRaiz("2","D","Ré","D","Ré");
		adicionaRaiz("3","D#","Ré sustenido","Eb","Mi bemol");
		adicionaRaiz("4","E","Mi","E","Mi");
		adicionaRaiz("5","F","Fá","F","Fá");
		adicionaRaiz("6","F#","Fá sustenido","Gb","Sol Bemol");
		adicionaRaiz("7","G","Sol","G","Sol");
		adicionaRaiz("8","G#","Sol sustenido","Ab","Lá bemol");
		adicionaRaiz("9","A","Lá","A","Lá");
		adicionaRaiz("10","A#","Lá sustenido","Bb","Si bemol");
		adicionaRaiz("11","B","Si","B","Si");
	}
	{ //Inserção de fórmulas de acordes
		adicionaFormula(".0.4.7.","Maior","");
		adicionaFormula(".0.3.7.","Menor","m");
		adicionaFormula(".0.4.8.","Aumentada","aug");
		adicionaFormula(".0.3.6.","Diminuta","dim");
		adicionaFormula(".0.3.6.9.","7ª diminuta","dim7");
		adicionaFormula(".0.2.7.","Suspenso de 2ª","sus2");
		adicionaFormula(".0.5.7.","Suspenso de 4ª","sus4");
		adicionaFormula(".0.4.7.14.","Com 9ª","add9");
		adicionaFormula(".0.3.7.14.","Menor com 9ª","madd9");
		adicionaFormula(".0.4.7.10.","7ª","7");
		adicionaFormula(".0.4.6.10.","7ª diminuta de 5ª","7-5");
		adicionaFormula(".0.4.8.10.","7ª aumentada de 5ª","7+5");
		adicionaFormula(".0.3.7.10.","Menor com 7ª","m7");
		adicionaFormula(".0.3.6.10.","Menor com 7ª diminuta de 5ª","m7-5");
		adicionaFormula(".0.3.8.10.","Menor com 7ª aumentada de 5ª","m7+5");
		adicionaFormula(".0.4.7.11.","7ª maior","maj7");
		adicionaFormula(".0.3.7.11.","Menor com 7ª maior","mmaj7");
		adicionaFormula(".0.4.7.11.14.","9ª maior","maj9");
		adicionaFormula(".0.3.7.11.14.","Menor com 9ª maior","mmaj9");
		adicionaFormula(".0.4.7.10.14.","9ª","9");
		adicionaFormula(".0.4.7.10.13.","9ª diminuta","-9");
		adicionaFormula(".0.3.7.10.14.","9ª menor","m9");
		adicionaFormula(".0.4.7.9.","6ª","6");
		adicionaFormula(".0.3.7.9.","menor com 6ª","m6");
		adicionaFormula(".0.4.7.9.14.","6ª e 9ª","6/9");
		adicionaFormula(".0.3.7.9.14.","menor com 6ª e 9ª","m6/9");
		adicionaFormula(".0.4.7.10.14.17.","11ª","11");
		adicionaFormula(".0.3.7.10.14.17.","menor com 11ª","m11");
		adicionaFormula(".0.4.7.11.14.17.","11ª maior","maj11");
		adicionaFormula(".0.3.7.11.14.17.","menor com 11ª maior","mmaj11");
		adicionaFormula(".0.4.7.10.14.17.21.","13ª","13");
		adicionaFormula(".0.3.7.10.14.17.21.","menor com 13ª","m13");
		adicionaFormula(".0.4.7.11.14.17.21.","13ª maior","maj13");
		adicionaFormula(".0.3.7.11.14.17.21.","menor com 13ª maior","mmaj13");
		adicionaFormula(".0.7.","simplificado com 5ª","5");
		adicionaFormula(".0.4.6.","com 5ª diminuta","-5");
		adicionaFormula(".0.2.4.7.","com 2ª","2");
	}
	{ //Carregamento de áudios
		carregaAudio("c3");
		carregaAudio("db3");
		carregaAudio("d3");
		carregaAudio("eb3");
		carregaAudio("e3");
		carregaAudio("f3");
		carregaAudio("gb3");
		carregaAudio("g3");
		carregaAudio("ab3");
		carregaAudio("a3");
		carregaAudio("bb3");
		carregaAudio("b3");
		carregaAudio("c4");
		carregaAudio("db4");
		carregaAudio("d4");
		carregaAudio("eb4");
		carregaAudio("e4");
		carregaAudio("f4");
		carregaAudio("gb4");
		carregaAudio("g4");
		carregaAudio("ab4");
		carregaAudio("a4");
		carregaAudio("bb4");
		carregaAudio("b4");
	}
	redimensionarTela();
	atualizaAcorde();
	console.log("Acordezemos inicializado!");
}
inicializaAcordezemos();

function adicionaRaiz(argFormula,argNota,argNome,argNotaBemol,argNomeBemol) {
	var novaFormula = document.createElement("option");
	novaFormula.value=raizes.length;
	if (argNota==argNotaBemol) {
		novaFormula.innerHTML=argNota;
	} else {
		novaFormula.innerHTML=argNota+" / "+argNotaBemol;
	}
	select_raizes.appendChild(novaFormula);
	select_raizes.value=0;
	raizes.push([argFormula,argNota,argNome,argNotaBemol,argNomeBemol]);
}

function adicionaFormula(argFormula,argAcorde,argNomenclatura) {
	var novaFormula = document.createElement("option");
	novaFormula.value=acordes.length;
	novaFormula.innerHTML=argAcorde;
	select_acordes.appendChild(novaFormula);
	select_acordes.value=0;
	acordes.push([argFormula,argAcorde,argNomenclatura]);
}

function atualizaAcorde() {
	//console.log(raizes[select_raizes.value]);
	//console.log(acordes[select_acordes.value]);
	if (input_usarBemol.checked) {
		div_acorde.innerHTML=raizes[select_raizes.value][3]+acordes[select_acordes.value][2];
		div_acordeNome.innerHTML="("+raizes[select_raizes.value][4]+" "+acordes[select_acordes.value][1].toLowerCase()+")";
		document.getElementById("tecla1").innerHTML="<span><div>Db3</div></span>";
		document.getElementById("tecla3").innerHTML="<span><div>Eb3</div></span>";
		document.getElementById("tecla6").innerHTML="<span><div>Gb3</div></span>";
		document.getElementById("tecla8").innerHTML="<span><div>Ab3</div></span>";
		document.getElementById("tecla10").innerHTML="<span><div>Bb3</div></span>";
		document.getElementById("tecla13").innerHTML="<span><div>Db4</div></span>";
		document.getElementById("tecla15").innerHTML="<span><div>Eb4</div></span>";
		document.getElementById("tecla18").innerHTML="<span><div>Gb4</div></span>";
		document.getElementById("tecla20").innerHTML="<span><div>Ab4</div></span>";
		document.getElementById("tecla22").innerHTML="<span><div>Bb4</div></span>";
	} else {
		div_acorde.innerHTML=raizes[select_raizes.value][1]+acordes[select_acordes.value][2];
		div_acordeNome.innerHTML="("+raizes[select_raizes.value][2]+" "+acordes[select_acordes.value][1].toLowerCase()+")";
		document.getElementById("tecla1").innerHTML="<span><div>C#3</div></span>";
		document.getElementById("tecla3").innerHTML="<span><div>D#3</div></span>";
		document.getElementById("tecla6").innerHTML="<span><div>F#3</div></span>";
		document.getElementById("tecla8").innerHTML="<span><div>G#3</div></span>";
		document.getElementById("tecla10").innerHTML="<span><div>A#3</div></span>";
		document.getElementById("tecla13").innerHTML="<span><div>C#4</div></span>";
		document.getElementById("tecla15").innerHTML="<span><div>D#4</div></span>";
		document.getElementById("tecla18").innerHTML="<span><div>F#4</div></span>";
		document.getElementById("tecla20").innerHTML="<span><div>G#4</div></span>";
		document.getElementById("tecla22").innerHTML="<span><div>A#4</div></span>";
	}
	limpaNota();
	for (i=0-parseInt(raizes[select_raizes.value][0]); i<=23-parseInt(raizes[select_raizes.value][0]); i++) {
		var stringAudio=null;
		switch (i+parseInt(raizes[select_raizes.value][0])) {
			case 0: stringAudio="c3"; break;7
			case 1: stringAudio="db3"; break;
			case 2: stringAudio="d3"; break;
			case 3: stringAudio="eb3"; break;
			case 4: stringAudio="e3"; break;
			case 5: stringAudio="f3"; break;
			case 6: stringAudio="gb3"; break;
			case 7: stringAudio="g3"; break;
			case 8: stringAudio="ab3"; break;
			case 9: stringAudio="a3"; break;
			case 10: stringAudio="bb3"; break;
			case 11: stringAudio="b3"; break;
			case 12: stringAudio="c4"; break;
			case 13: stringAudio="db4"; break;
			case 14: stringAudio="d4"; break;
			case 15: stringAudio="eb4"; break;
			case 16: stringAudio="e4"; break;
			case 17: stringAudio="f4"; break;
			case 18: stringAudio="gb4"; break;
			case 19: stringAudio="g4"; break;
			case 20: stringAudio="ab4"; break;
			case 21: stringAudio="a4"; break;
			case 22: stringAudio="bb4"; break;
			case 23: stringAudio="b4"; break;
		}
		document.getElementById("valor"+(i+parseInt(raizes[select_raizes.value][0]))).innerHTML="<span class=\"valorAcorde\"><div>"+i+"</div></span>";
		var notaAtual=document.getElementById("tecla"+(i+parseInt(raizes[select_raizes.value][0])));
		var preenchimento=document.getElementById("tecla"+(i+parseInt(raizes[select_raizes.value][0]))+"preenche");
		var notaAudio=document.getElementById("audio_piano_"+stringAudio);
		notaAudio.pause();
		notaAudio.currentTime=0;
		if (acordes[select_acordes.value][0].includes("."+i+".")) {
			notaAudio.play();
			notaAtual.classList.add("marcada");
			criaNota(notaAtual.childNodes[0].childNodes[0].innerHTML);
			if (preenchimento!==null) {
				preenchimento.classList.add("marcada");
			}
		} else {
			notaAtual.classList.remove("marcada");
			if (preenchimento!==null) {
				preenchimento.classList.remove("marcada");
			}
		}
	}
	var formulaAcorde=acordes[select_acordes.value][0];
	formulaAcorde=formulaAcorde.slice(1);
	formulaAcorde=formulaAcorde.slice(0,-1);
	while (formulaAcorde.includes(".")) {
		formulaAcorde=formulaAcorde.replace(".","-");
	}
	span_formulaAcorde.innerHTML=formulaAcorde;
}
function criaNota(argNota) {
	var partituraPosicaoY;
	switch (select_claves.value) {
		case "G": partituraPosicaoY=75;
		case "F": partituraPosicaoY=50;
		case "C": partituraPosicaoY=80;
	}
	var acidente="";
	switch (argNota.charAt(0)) {
		case "C": partituraPosicaoY+=30; break;
		case "D": partituraPosicaoY+=25; break;
		case "E": partituraPosicaoY+=20; break;
		case "F": partituraPosicaoY+=15; break;
		case "G": partituraPosicaoY+=10; break;
		case "A": partituraPosicaoY+=5; break;
		case "B": partituraPosicaoY+=0; break;
	}
	switch (argNota.charAt(1)) {
		case "2": partituraPosicaoY+=35; break;
		case "3": partituraPosicaoY+=0; break;
		case "4": partituraPosicaoY+=-35; break;
		case "#":
		case "b": {
			acidente=argNota.charAt(1);
			switch (argNota.charAt(2)) {
				case "2": partituraPosicaoY+=35; break;
				case "3": partituraPosicaoY+=0; break;
				case "4": partituraPosicaoY+=-35; break;
			}
		}; break;
	}
	var partituraNota=document.createElement("img");
	partituraNota.classList.add("nota");
	partituraNota.setAttribute("src","gfx/partitura_figSb.png");
	if (partituraPosicaoY>=105) {
		partituraNota.style.background="url('gfx/partitura_pauta.png') no-repeat";
		if (partituraPosicaoY%10==0) {
			partituraNota.style.backgroundPosition="center 30px";
		} else {
			partituraNota.style.backgroundPosition="center 35px";
		}
	} else if (partituraPosicaoY<=45) {
		partituraNota.style.background="url('gfx/partitura_pauta.png') no-repeat";
		if (partituraPosicaoY%10==0) {
			partituraNota.style.backgroundPosition="center 80px";
		} else {
			partituraNota.style.backgroundPosition="center 75px";
		}
	}
	partituraNota.style.top=partituraPosicaoY+"px";
	var partituraAcidente=null;
	if (acidente=="") {
		partituraNota.style.left="18px";
	} else {
		partituraNota.style.left="5px";
		partituraAcidente=document.createElement("img");
		partituraAcidente.classList.add("nota");
		if (acidente=="#") {
			partituraAcidente.setAttribute("src","gfx/partitura_acidente+.png");
		} else if (acidente=="b") {
			partituraAcidente.setAttribute("src","gfx/partitura_acidente-.png");
		}
		partituraAcidente.style.top=partituraPosicaoY+"px";
		partituraAcidente.style.left="10px";
	}
	div_partituraCompasso.appendChild(partituraNota);
	if (partituraAcidente!==null) {
		div_partituraCompasso.appendChild(partituraAcidente);
	}
}
function limpaNota() {
	div_partituraCompasso.innerHTML="";
	var partituraClave=document.createElement("img");
	partituraClave.id="partitura_clave";
	partituraClave.setAttribute("src","gfx/partitura_clave"+select_claves.value+".png");
	partituraClave.classList.add("clave");
	div_partituraCompasso.appendChild(partituraClave);
}
function alternarExibicao() {
	if (exibicao=="teclado") {
		exibicao="partitura";
		div_partitura.style.display="block";
		table_teclado.style.display="none";
	} else if (exibicao=="partitura") {
		exibicao="teclado";
		div_partitura.style.display="none";
		table_teclado.style.display="inline-block";
	}
}
function showMeDaWae() {
	var texto="<h1>\"Fórmula do acorde\"?</h1>";
	texto+="<p>Em teoria, cada acorde tem a sua sequência certa de notas a serem executadas... E, independente da raiz do acorde, essa sequência não muda.</p>";
	texto+="<p>Vamos supor que queremos obter sempre o 1º harmônico e o 5º harmônico de um acorde qualquer. Desse modo, podemos tomar o 1º harmônico como se fosse o nosso \"valor 0\". Em seguida, vamos aumentando esse número de semitom em semitom, até chegar no valor referente ao 5º harmônico. Vamos ao exemplo:</p>";
	texto+="<p>Nota C é o valor 0. O 5º harmônico dessa nota é a nota G. Contando da nota C, nós devemos subir 7 semitons para chegar na nota G. <b>Então, a fórmula para executar o 1º e o 5º harmônico é: 0-7</b></p>";
	texto+="<p>Então, através dessa fórmula, podemos saber qual é o 5º harmônico de qualquer nota se nós começarmos por ela e subir 7 semitons. 0-7, lembra?</p>";
	texto+="<p>Com esse tipo de fórmula baseado em semitons a partir da raiz, podemos descobrir as notas que formam qualquer acorde. É perfeito para quem está aprendendo acordes ou não tem certeza se uma determinada nota pertence ao acorde!</p>";
	texto+="<p>Outros exemplos de fórmulas de acorde:</p>";
	texto+="<ul>";
	texto+="<li>Acorde em maior: 0-4-7</li>";
	texto+="<li>Acorde em menor: 0-3-7</li>";
	texto+="<li>Acorde em sétima: 0-4-7-10</li>";
	texto+="<li>Acorde em sétima maior: 0-4-7-11</li>";
	texto+="</ul>";
	texto+="<p>Obs.: Não saia espalhando esse termo por aí não, pois não sei se ele existe de verdade (!). Apenas inventei e não chequei! Não sou músico profissional e, se isso é realmente documentado e praticado por aí, alguém por favor me avise!</p>";
	div_descricao.innerHTML=texto;
}

function redimensionarTela() {
	console.log("Resolução alterada");
	if (document.body.clientWidth<=1000) {
		select_acordes.setAttribute("size",1);
		select_raizes.setAttribute("size",1);
	} else {
		select_acordes.setAttribute("size",24);
		select_raizes.setAttribute("size",24);
	}
}

function carregaAudio(argAudio) {
	var audio=document.createElement("audio");
	var sourcemp3=document.createElement("source");
	var sourceogg=document.createElement("source");
	var sourcewav=document.createElement("source");
	sourcemp3.setAttribute("src","sfx/piano_"+argAudio+".mp3");
	sourceogg.setAttribute("src","sfx/piano_"+argAudio+".ogg");
	sourcewav.setAttribute("src","sfx/piano_"+argAudio+".wav");
	audio.appendChild(sourcemp3);
	audio.appendChild(sourceogg);
	audio.appendChild(sourcewav);
	audio.id="audio_piano_"+argAudio;
	document.body.appendChild(audio);
}
