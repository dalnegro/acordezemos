# Acordezemos

Um pequeno programinha identificador de acordes, através de "fórmulas de acordes".

Versão utilizável disponível em [http://leandrogabriel.net/sistemas/acordezemos/](http://leandrogabriel.net/sistemas/acordezemos/)

## E o que tem de inteligente nisso?

Em teoria, cada acorde musical tem uma "sequência pré-definida" para as notas específicas a serem executadas dentro do tom.
Em suma, este programa pega essas sequências (as quais eu amadoramente as chamo de "fórmulas de acordes"), baseadas em semitons-acima-da-nota-raiz-do-acorde e marca as notas que devem ser executadas dentro daquele acorde.

## Ok, agora explica po pai o que realmente acontece...

A cada nota, é atribuído um valor, começando em 0, que é a nota "raiz do acorde". Por exemplo, começando da raiz C, então C será 0.
A cada semitom acima é somado esse valor. O resultado seriam os seguintes valores para cada semitom acima:

| 0 | 1  | 2 | 3  | 4 | 5 | 6  | 7 | 8  | 9 | 10 | 11 | 12 | 13 | ... |
| - | -- | - | -- | - | - | -- | - | -- | - | -- | -- | -- | -- | --- |
| C | C# | D | D# | E | F | F# | G | G# | A | A# | B  | C  | C# | ... |

Com isso, assim que obtemos esses valores, aplicamos a "fórmula do acorde" da tríade comum maior, que é 0-4-7. Deste modo, destacamos as notas com os valores 0, 4 e 7:

| **0** | **4** | **7** |
| ----- | ----- | ----- |
| **C** | **E** | **G** |

E, com essa mesma fórmula do 0-4-7, podemos aplicá-la **em qualquer outra raiz**, que o resultado serão as notas referentes à tríade comum maior de quaisquer acordes

| **0** | **4**  | **7** |
| ----- | ------ | ----- |
| **D** | **F#** | **A** |
| **E** | **G#** | **B** |
| **F** | **A**  | **C** |
| **G** | **B**  | **D** |
| ...   | ...    | ...   |

E é isso que o programa faz: Pega o valor da raiz, e varre cada uma das teclas dos semitons acima, pra verificar se o valor das teclas está dentro do acorde, e destaca elas.
Assim fica fácil de identificar quais notas pertencem a qual acorde, bastando apenas suprir a informação que o programa precisa.
Acima e abaixo do teclado do programa é exibido esses valores, para conferir melhor a fórmula do acorde. Note que, quando se troca a raiz, os valores mudam, mas a fórmula permanece a mesma!

Bons estudos!